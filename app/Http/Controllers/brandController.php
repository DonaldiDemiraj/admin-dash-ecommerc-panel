<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class brandController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'title'=>'required|unique:brands|max:255'
        ]);

        DB::table('brands')->insert([
            'title'=>$request->title,
        ]);
       
        session()->put('status','Brand Added Successfully!');
        // dd(session('status'));
   return view('brand.add');
    }

    public function all()
    {
        $brands=DB::table('brands')->get();
        return view('brand.allBrands')->with(['brands' =>$brands]);
    }

    public function delete($id)
    {
        DB::table('brands')->delete($id);
        session()->put('status','Brand Deleted Successfully!');
        return redirect('allBrand');
    }

    public function edit($id)
    {
        $brands = DB::table('brands')->find($id); 
        return view('brand.update')->with('brand',$brands);
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'title'=>'required|unique:brands|max:255'
        ]);

        DB::table('brands')->where('id',$id)->update([
            'title'=>$request->title,
        ]);
        session()->put('status','Brand Updated Successfully!');
        // dd(session('status'));
        return redirect('allBrand');
    }
}
