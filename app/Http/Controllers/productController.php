<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class productController extends Controller
{
    public function add(Request $request)
    {
        $request->validate([
            'name'=>'required|unique:products|max:255',
            'price'=>'required',
            'desc'=>'required',
            'cat'=>'required',
            'brand'=>'required'
        ]);

        DB::table('products')->insert([
            'name'=>$request->name,
            'price'=>$request->price,
            'desc'=>$request->desc,
            'cat'=>$request->cat,
            'brand'=>$request->brand,
        ]);
       
        session()->put('status','Product Added Successfully!');
        // dd(session('status'));
   return view('product.add');
    }

    public function all()
    {
        $products=DB::table('products')->get();
        return view('product.allProducts')->with(['products' =>$products]);
    }

    public function delete($id)
    {
        DB::table('products')->delete($id);
        session()->put('status','Product Deleted Successfully!');
        return redirect('allProduct');
    }

    public function edit($id)
    {
        $product = DB::table('products')->find($id); 
        return view('product.update')->with('product',$product);
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'name'=>'required|unique:products|max:255',
            'price'=>'required',
            'desc'=>'required',
            'cat'=>'required',
            'brand'=>'required'
        ]);

        $prod = DB::table('products')->where('id',$id)->update([
            'name'=>$request->name,
            'price'=>$request->price,
            'desc'=>$request->desc,
            'cate'=>$request->cate,
            'brand'=>$request->brand
        ]);
       
        session()->put('status','Product Updated Successfully!');
        // dd(session('status'));
        return redirect('allProduct');
    }
}
