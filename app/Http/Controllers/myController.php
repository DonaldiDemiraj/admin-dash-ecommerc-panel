<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class myController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request,$next)
        {
            if(($request->name!="yousaf")
            {
                return response("You are not Authorized");
            }
            
            return $next($request);
            
        })->expect(['display']);
    }
}
