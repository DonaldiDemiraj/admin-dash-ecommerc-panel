@extends('admin.adminLayout')
@section('content')


    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
                <strong>Error!</strong> {{$error}}
                {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
            </div>
        @endforeach
    @endif

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif
  {{-- {{asset('updateProduct/$product->id')}} --}}
    <form role="form" action="{{asset('updateBrand/$brand->id')}}" method="POST">
        @csrf

        <div class="card-body">
            <div class="form-group">
                <label for="name">Title</label>
                <input value="{{$brand->title}}" name="title" type="text" class="form-control" id="title" placeholder="Brand title">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update Brand</button>
        </div>
    </form> 
@endsection
