@extends('admin.adminLayout')
@section('content')

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif

    @foreach ($errors->all() as $error)
       <div style="padding: 10px;" class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error!</strong> {{$error}}
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
      </div>
    @endforeach

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
          </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif

    <form role="form" action="{{url('./addBrand')}}" method="POST">
        {{method_field('post')}}
        @csrf

        <div class="card-body">
            <div class="form-group">
                <label for="name">Title</label>
                <input name="title" type="text" class="form-control" id="title" placeholder="Brand title">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add Product</button>
        </div>
    </form> 
@endsection
