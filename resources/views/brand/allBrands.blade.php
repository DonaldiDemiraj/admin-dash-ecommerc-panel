@extends('admin.adminLayout')
@section('content')

@if (session('status'))
<div style="padding: 10px;" class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Message!</strong> {{session('status')}}
    {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">x</button> --}}
</div>
<?php  
    session()->forget('status'); 
?>
@endif


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">All Peroducts</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($brands as $b)
                        <tr>
                            <td>{{$b->id}}</td>
                            <td>{{$b->title}}</td>
                            <td><a href="deleteBrand/{{$b->id}}">Delete</a> | <a href="editBrand/{{$b->id}}">Edit</a></td>
                        </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection