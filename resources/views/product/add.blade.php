@extends('admin.adminLayout')
@section('content')

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif

    @foreach ($errors->all() as $error)
       <div style="padding: 10px;" class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error!</strong> {{$error}}
        {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
      </div>
    @endforeach

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
          </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif

    <form role="form" action="{{url('./addProduct')}}" method="POST">
        {{method_field('post')}}
        @csrf

        <div class="card-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" id="name" placeholder="Product name">
            </div>
            <div class="form-group">
                <label for="name">Price</label>
                <input name="price" type="text" class="form-control" id="name" placeholder="Product Price">
            </div>
            <div class="form-group">
                <label for="name">Description</label>
                <textarea name="desc" style="height: 100px;" id="" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="name">Brands</label>
                <select name="brand" class="form-control">
                    <option value="b1">Brand-1</option>
                    <option value="c">Brand-2</option>
                    <option value="b3">Brand-3</option>
                </select>
            </div>
            <div class="form-group">
                <label for="cat">Category</label>
                <select name="cat" class="form-control">
                    <option value="c1">Category-1</option>
                    <option value="c2">Category-2</option>
                    <option value="c3">Category-3</option>
                </select>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add Product</button>
        </div>
    </form> 
@endsection
