@extends('admin.adminLayout')
@section('content')

@if (session('status'))
<div style="padding: 10px;" class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Message!</strong> {{session('status')}}
    {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">x</button> --}}
</div>
<?php  
    session()->forget('status'); 
?>
@endif


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">All Peroducts</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Brand</th>
                        <th>Category</th>
                        <th>Desc</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $p)
                        <tr>
                            <td>{{$p->name}}</td>
                            <td>{{$p->price}}</td>
                            <td>{{$p->brand}}</td>
                            <td>{{$p->cate}}</td>
                            <td>{{$p->desc}}</td>
                            <td><a href="deleteProduct/{{$p->id}}">Delete</a> | <a href="editProduct/{{$p->id}}">Edit</a></td>
                        </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Brand</th>
                        <th>Category</th>
                        <th>Desc</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection