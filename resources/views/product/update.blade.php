@extends('admin.adminLayout')
@section('content')


    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
                <strong>Error!</strong> {{$error}}
                {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
            </div>
        @endforeach
    @endif

    @if (session('status'))
        <div style="padding: 10px;" class="alert alert-error alert-dismissible fade show" role="alert">
            <strong>Message!</strong> {{session('status')}}
            {{-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> --}}
        </div>
        <?php  
            session()->forget('status'); 
        ?>
    @endif
  {{-- {{asset('updateProduct/$product->id')}} --}}
    <form role="form" action="{{asset('updateProduct/$product->id')}}" method="POST">
        @csrf

        <div class="card-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="{{$product->name}}" name="name" type="text" class="form-control" id="name" placeholder="Product name">
            </div>
            <div class="form-group">
                <label for="name">Price</label>
                <input value="{{$product->price}}" name="price" type="text" class="form-control" id="name" placeholder="Product Price">
            </div>
            <div class="form-group">
                <label for="name">Description</label>
                <textarea name="desc" style="height: 100px;" id="" cols="30" rows="10">{{$product->desc}}</textarea>
            </div>
            <div class="form-group">
                <label for="name">Brands</label>
                <select name="brand" class="form-control">
                    <option value="{{$product->brand}}">{{$product->brand}}</option>    
                </select>
            </div>
            <div class="form-group">
                <label for="cat">Category</label>
                <select name="cat" class="form-control">
                    <option value="{{$product->cate}}">{{$product->cate}}</option>
                </select>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update Product</button>
        </div>
    </form> 
@endsection
