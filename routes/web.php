<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\productController;
use App\Http\Controllers\brandController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::view('my','admin.adminLayout');

route::view('addView','product.add');


//for request
Route::post('addProduct',[productController::class, 'add']);
Route::any('allProduct',[productController::class, 'all']);
Route::any('deleteProduct/{id}',[productController::class, 'delete']);
Route::any('editProduct/{id}',[productController::class, 'edit']);
Route::any('updateProduct/{id}',[productController::class, 'update']);



route::view('brandAddView','brand.add');
//for brand
Route::post('addBrand',[brandController::class, 'add']);
Route::any('allBrand',[brandController::class, 'all']);
Route::any('deleteBrand/{id}',[brandController::class, 'delete']);
Route::any('editBrand/{id}',[brandController::class, 'edit']);
Route::any('updateBrand/{id}',[brandController::class, 'update']);
